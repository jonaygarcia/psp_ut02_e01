# Problema del consumidor

## Enunciado

En un cierto programa se tienen procesos que producen números y procesos que leen esos números. Todos los números se introducen en una cola (o vector) limitada.

Todo el mundo lee y escribe de/en esa cola. Cuando un productor quiere poner un número tendrá que comprobar si la cola está llena. Si está llena, espera un tiempo al azar. Si no está llena pone su número en la última posición libre.

Cuando un lector quiere leer, examina si la cola está vacía. Si lo está espera un tiempo al azar, y sino coge el número que haya al principio de la cola y ese número ya no está disponible para el siguiente.

Crear un programa que simule el comportamiento de estos procesos evitando problemas de entrelazado e inanición.

Para la creación del ejercicio debemos crear 4 clases:

* Buffer.
* Productor.
* Consumidor.
* Main.

## Threads

Los __Hilos__ o __Threads__ en Java, son básicamente una forma de poder ejecutar varios procesos simultáneamente en nuestros programas en Java.

Para poder utilizarlos tenemos que crear clases que extienden de la clase _Thread_, y reescribir el metodo principal __run()__, el cual es el que se va a ejecutar principalmente al iniciar un hilo, thread o nuevo proceso en Java

![img_01][img_01]

Estados de un Thread:

* __Runnable__:  Un _Thread_ se vuelve ejecutable cuando se invoca al método __Thread.start()__. No significa que el _Thread_ se vaya a ejecutar directamente, sino que espera a que el planificador seleccione su ejecución en función de su prioridad.

* __Running__: Un _Thread_ comienza su ejecución cuando el planificador lo selecciona. Se ejecuta hasta que se bloquea o voluntariamente abandona su turno de ejecución mediante el método __Thread.yield()__.

* __Waiting__: Un _Thread_ está bloqueado mientras espera que termine algún proceso externo como puede ser archivo E/S. Una llamada al método __Object.wait()__ actual hace que el _Thread_ actual espere hasta que otro subproceso invoque el método __Object.notify()__ o __Object.notifyAll()__.

* __Sleeping__: Los threads de Java son forzados a dormir (suspendido) durante un tiempo especificado con este método: _Thread.sleep(milliseconds)_, _Thread.sleep(milliseconds, nanoseconds)__.

* __Dead__: El Thread ha finalizado su trabajo.

## Semáforos

Los semáforos se emplean para permitir el acceso a diferentes partes de programas (llamados secciones críticas) donde se manipulan variables o recursos que deben ser accedidos de forma especial. Según el valor con que son inicializados se permiten a más o menos procesos utilizar el recurso de forma simultánea.

![img_02][img_02]

Un tipo simple de semáforo es el binario, que puede tomar solamente los valores 0 y 1. Se inicializan en 1 y son usados cuando sólo un proceso puede acceder a un recurso a la vez. Se conocen como __mutex__:
* Cuando el recurso está disponible, un proceso accede y decrementa el valor del semáforo. El valor queda entonces en 0, lo que hace que si otro proceso intenta decrementarlo tenga que esperar.
* Cuando el proceso que decrementó el semáforo libera el semáforo, otro proceso que estaba esperando comienza a utilizar el recurso.

![img_03][img_03]


Enlace a la documentación de Oracle sobre _semáforos_:
* [https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Semaphore.html](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Semaphore.html)

## Clase Buffer

```java
import java.util.concurrent.*;

public class Buffer {

    private int[] buffer; // Array de enteros que simula la cola
    private int i = 0;    // Siguiente posicion donde tiene que colocar el productor
    private int j = 0;    // Siguiente posiciion donde tiene que coger el consumidor

    // Semaforo Mutex: Evitar que el buffer sea modificado por el productor y el consumidor a la vez.
    private Semaphore mutex = new Semaphore(1, true);

    // Semaforo hayDatos: Evita que el Consumidor no recoja productos del buffer cuando éste esté vacío.
    private Semaphore hayDatos = new Semaphore(0, true);

    // Semáforo hayEspacio: Evita que el Productor ponga productos en el buffer cuando éste esté lleno.
    private Semaphore hayEspacio;

    public Buffer(int tam) {
        buffer = new int[tam];
        hayEspacio = new Semaphore(buffer.length, true);
    }

    public void poner(int dato) throws InterruptedException {
        hayEspacio.acquire();
        mutex.acquire();

        buffer[i] = dato;
        //System.out.println("Productor produce " + dato);
        i = (i+1) % buffer.length;

        mutex.release();
        hayDatos.release();
    }

    public int extraer() throws InterruptedException {
        hayDatos.acquire();
        mutex.acquire();

        int aux = j; //Sitio de donde puedo extraer.
        j = (j+1) % buffer.length;
        //System.out.println("Consumidor consume " + buffer[aux]);

        mutex.release();
        hayEspacio.release();

        return buffer[aux];
    }
}
```

Esta clase contiene tres semáforos:
* __mutex__: controla que la estructura del buffer no puedan utilizarla el _productor_ y el _consumidor_ a la vez. Se inicializa a 1.
* __hayDatos__: controla que el _productor_ no pueda añadir productos si el buffer está lleno. Se inicializa a 0.
* __hayEspacio__: controla que el _consumidor_ no pueda coger productos si el buffer está vacío. Se inicializa en el constructor con el tamaño del buffer.

Existen tres métodos en la clase Buffer:
* __constructor__: crea un array de enteros que simulará dónde se ponen los productos e inicializa el semáforo hayEspacio con el tamño de la estructura buffer.
* __poner__: permite al _productor_ añadir un producto al buffer.
* __extraer__: permite al _recolector_ coger un producto del buffer. Devuelve el elemento que ha extraido del buffer.

La clase semáforo tiene entre otros, dos métodos:

* acquire: obtiene el semáforo si el contador es mayor que 0 y decrementa en 1 el contador del semáforo.
* release: libera el semáforo e incrementa en 1 el contador del semáforo.


Clase Productor

```java
public class Productor extends Thread {

    private Random r = new Random();
    private Buffer buffer;
    private int iteraciones;

    public Productor(Buffer buffer, int iteraciones) {
        this.buffer = buffer;
        this.iteraciones = iteraciones;
    }

    @Override
    public void run() {
        for (int i = 0; i < iteraciones; i++) {
            int aux = r.nextInt(100);
            System.out.println(i+": Productor produce " + aux);
            try {
                buffer.poner(aux);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
```

La clase __Productor__ hereda de la clase _Thread_ Tiene un sólo método:

* __run()__: contiene un bucle que genera un número aleatorio entre 0 y 99 que simula el producto e intenta ponerlo en el _buffer_.

## Clase Consumidor

```java
public class Consumidor extends Thread {

    private Buffer buffer;
    private int iteraciones;

    public Consumidor(Buffer buffer, int iteraciones) {
        this.buffer = buffer;
        this.iteraciones = iteraciones;
    }

    @Override
    public void run() {
        for (int i = 0; i < iteraciones; i++) {
            try {
                int aux = buffer.extraer();
                System.out.println(i + ": Consumidor consume " + aux);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
```

La clase __Consumidor__ hereda de la clase _Thread_ Tiene un sólo método:

* __run()__: contiene un bucle en el que cada iteración intenta extraer un elemento del  _buffer_.


## Clase Main

```java
public class Main {

    public static void main(String[] args) {

        Buffer buffer = new Buffer(5);
        Productor productor = new Productor(buffer, 100);
        Consumidor consumidor = new Consumidor(buffer, 100);

        productor.start();
        consumidor.start();

    }

}
```

La clase __Main__ simplemente crea los siguientes objetos:
* __buffer__: lugar donde se van a colocar los productos creados por el _productor_. Tiene un tamaño de 6.
* __productor__: crea un _productor_ que va a producir 100 productos.
* __consumidor__: crea un _consumidor_ que va a consumir 100 productos.

Posteriormente ejecuta los métodos _run()_, tanto del _productor_ como del _consumidor_ mediante las llamadas al método _start()_ de cada uno de los objetos.

## Salida generada

Al ejecutar la aplicación generamos la siguiente salida:

```bash
0: Productor produce 25
1: Productor produce 58
2: Productor produce 25
3: Productor produce 69
0: Consumidor consume 25
1: Consumidor consume 58
4: Productor produce 85
5: Productor produce 76
6: Productor produce 12
2: Consumidor consume 25
7: Productor produce 0
8: Productor produce 91
9: Productor produce 9
3: Consumidor consume 69
4: Consumidor consume 85
10: Productor produce 75
11: Productor produce 60
5: Consumidor consume 76
6: Consumidor consume 12
7: Consumidor consume 0
8: Consumidor consume 91
12: Productor produce 69
13: Productor produce 46
14: Productor produce 14
15: Productor produce 60
9: Consumidor consume 9
10: Consumidor consume 75
16: Productor produce 93
11: Consumidor consume 60
17: Productor produce 67
12: Consumidor consume 69
18: Productor produce 13
13: Consumidor consume 46
19: Productor produce 25
14: Consumidor consume 14
15: Consumidor consume 60
20: Productor produce 43
21: Productor produce 8
22: Productor produce 34
16: Consumidor consume 93
17: Consumidor consume 67
23: Productor produce 87
24: Productor produce 90
18: Consumidor consume 13
19: Consumidor consume 25
25: Productor produce 60
20: Consumidor consume 43
26: Productor produce 83
21: Consumidor consume 8
27: Productor produce 56
22: Consumidor consume 34
28: Productor produce 24
23: Consumidor consume 87
29: Productor produce 64
24: Consumidor consume 90
30: Productor produce 89
25: Consumidor consume 60
31: Productor produce 35
26: Consumidor consume 83
32: Productor produce 49
27: Consumidor consume 56
33: Productor produce 29
28: Consumidor consume 24
34: Productor produce 12
29: Consumidor consume 64
30: Consumidor consume 89
35: Productor produce 2
31: Consumidor consume 35
36: Productor produce 39
32: Consumidor consume 49
37: Productor produce 78
33: Consumidor consume 29
34: Consumidor consume 12
38: Productor produce 35
35: Consumidor consume 2
36: Consumidor consume 39
37: Consumidor consume 78
38: Consumidor consume 35
39: Productor produce 97
40: Productor produce 57
41: Productor produce 88
39: Consumidor consume 97
42: Productor produce 32
43: Productor produce 4
44: Productor produce 89
45: Productor produce 27
40: Consumidor consume 57
41: Consumidor consume 88
46: Productor produce 5
42: Consumidor consume 32
43: Consumidor consume 4
44: Consumidor consume 89
45: Consumidor consume 27
47: Productor produce 71
46: Consumidor consume 5
48: Productor produce 41
47: Consumidor consume 71
48: Consumidor consume 41
49: Productor produce 2
50: Productor produce 82
51: Productor produce 64
49: Consumidor consume 2
52: Productor produce 34
50: Consumidor consume 82
53: Productor produce 16
51: Consumidor consume 64
54: Productor produce 63
52: Consumidor consume 34
55: Productor produce 5
53: Consumidor consume 16
56: Productor produce 64
57: Productor produce 11
58: Productor produce 75
59: Productor produce 61
60: Productor produce 3
54: Consumidor consume 63
55: Consumidor consume 5
56: Consumidor consume 64
57: Consumidor consume 11
58: Consumidor consume 75
59: Consumidor consume 61
61: Productor produce 54
62: Productor produce 59
63: Productor produce 87
64: Productor produce 82
65: Productor produce 78
60: Consumidor consume 3
61: Consumidor consume 54
62: Consumidor consume 59
63: Consumidor consume 87
64: Consumidor consume 82
66: Productor produce 93
67: Productor produce 18
68: Productor produce 5
69: Productor produce 33
70: Productor produce 76
65: Consumidor consume 78
66: Consumidor consume 93
67: Consumidor consume 18
68: Consumidor consume 5
71: Productor produce 63
69: Consumidor consume 33
72: Productor produce 88
70: Consumidor consume 76
73: Productor produce 96
71: Consumidor consume 63
74: Productor produce 4
72: Consumidor consume 88
75: Productor produce 83
73: Consumidor consume 96
74: Consumidor consume 4
75: Consumidor consume 83
76: Productor produce 11
77: Productor produce 19
76: Consumidor consume 11
78: Productor produce 91
79: Productor produce 88
80: Productor produce 4
77: Consumidor consume 19
78: Consumidor consume 91
81: Productor produce 3
82: Productor produce 43
83: Productor produce 65
84: Productor produce 72
79: Consumidor consume 88
80: Consumidor consume 4
85: Productor produce 67
86: Productor produce 8
87: Productor produce 83
81: Consumidor consume 3
82: Consumidor consume 43
83: Consumidor consume 65
84: Consumidor consume 72
85: Consumidor consume 67
86: Consumidor consume 8
88: Productor produce 50
89: Productor produce 8
90: Productor produce 3
91: Productor produce 80
92: Productor produce 57
87: Consumidor consume 83
88: Consumidor consume 50
89: Consumidor consume 8
90: Consumidor consume 3
91: Consumidor consume 80
93: Productor produce 96
94: Productor produce 74
95: Productor produce 19
96: Productor produce 63
97: Productor produce 10
92: Consumidor consume 57
93: Consumidor consume 96
94: Consumidor consume 74
95: Consumidor consume 19
96: Consumidor consume 63
98: Productor produce 42
99: Productor produce 56
97: Consumidor consume 10
98: Consumidor consume 42
99: Consumidor consume 56
```

[img_01]: img/01.png "Hilos"
[img_02]: img/02.png "Semáforos"
[img_03]: img/03.png "Semáforo Mutex"
